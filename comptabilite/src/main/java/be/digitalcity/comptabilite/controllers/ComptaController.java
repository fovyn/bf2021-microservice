package be.digitalcity.comptabilite.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ComptaController {

    @GetMapping(path = {"", "/", "/index"})
    public String indexAction() {

        return "compta/index";
    }
}
